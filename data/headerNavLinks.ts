const headerNavLinks = [
  { href: '/blogs', title: 'Blog' },
  { href: '/tags', title: 'Tags' },
  // { href: '/projects', title: 'Projects' },
  { href: '/authors/hlchanad', title: 'About' },
]

export default headerNavLinks
