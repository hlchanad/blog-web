import { ApolloClient, InMemoryCache } from '@apollo/client'
import { config } from '@/helpers'

export const client = new ApolloClient({
  uri: config('graphql.url'),
  cache: new InMemoryCache(),
  defaultOptions: {
    query: {
      fetchPolicy: 'no-cache',
    },
    watchQuery: {
      fetchPolicy: 'no-cache',
    },
  },
})
