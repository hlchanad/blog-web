import { gql } from '@apollo/client'
import { client } from '../../apollo-client'
import { Post } from '../../types'

const query = gql`
  query Post($slug: String!) {
    post(slug: $slug) {
      id
      title
      content
      summary
      slug
      banner
      author {
        id
        username
        firstname
        lastname
        avatar
        email
      }
      tags {
        name
      }
      createdAt
      updatedAt
      previous {
        slug
        title
      }
      next {
        slug
        title
      }
    }
  }
`

export async function PostQuery(slug: string): Promise<Post> {
  const { data } = await client.query<{ post: Post }>({ query, variables: { slug } })
  return data.post
}
