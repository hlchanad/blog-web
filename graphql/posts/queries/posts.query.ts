import { gql } from '@apollo/client'
import { client } from '../../apollo-client'
import { PostPageable, QueryPostsArgs } from '../../types'

const query = gql`
  query Posts($filter: PostsFilterQueryInput!, $sort: PostsSortEnum!, $offset: Int!, $limit: Int!) {
    posts(filter: $filter, sort: $sort, offset: $offset, limit: $limit) {
      edges {
        id
        title
        summary
        slug
        tags {
          name
        }
        createdAt
        updatedAt
      }
      totalCount
      pageInfo {
        hasNextPage
      }
    }
  }
`

export async function PostsQuery(args?: QueryPostsArgs): Promise<PostPageable> {
  const { data } = await client.query<{ posts: PostPageable }>({
    query,
    variables: {
      filter: args?.filter ?? {},
      offset: args?.offset ?? 0,
      limit: args?.limit ?? 5,
      sort: args?.sort ?? 'LATEST_CREATED',
    } as QueryPostsArgs,
  })
  return data.posts
}
