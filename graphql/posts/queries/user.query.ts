import { gql } from '@apollo/client'
import { client } from '../../apollo-client'
import { User } from '../../types'

const query = gql`
  query User($username: String!) {
    user(username: $username) {
      id
      username
      firstname
      lastname
      avatar
      email
      contacts {
        type
        url
      }
      description
    }
  }
`

export async function UserQuery(username: string): Promise<User> {
  const { data } = await client.query<{ user: User }>({ query, variables: { username } })
  return data.user
}
