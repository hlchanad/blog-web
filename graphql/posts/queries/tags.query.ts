import { gql } from '@apollo/client'
import { client } from '../../apollo-client'
import { QueryTagsArgs, TagPageable } from '../../types'

const query = gql`
  query Tags($filter: TagsFilterInput!, $sort: TagsSortEnum!, $offset: Int!, $limit: Int!) {
    tags(filter: $filter, sort: $sort, offset: $offset, limit: $limit) {
      edges {
        id
        name
        slug
        stat {
          count
        }
      }
    }
  }
`

export async function TagsQuery(args?: QueryTagsArgs): Promise<TagPageable> {
  const { data } = await client.query<{ tags: TagPageable }>({
    query,
    variables: {
      filter: args?.filter ?? {},
      offset: args?.offset ?? 0,
      limit: args?.limit ?? 10,
      sort: args?.sort ?? 'POPULAR',
    } as QueryTagsArgs,
  })
  return data.tags
}
