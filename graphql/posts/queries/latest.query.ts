import { PostPageable, PostsSortEnum } from '../../types'
import { PostsQuery } from './posts.query'

export async function LatestQuery(limit = 5): Promise<PostPageable> {
  return PostsQuery({ limit, sort: 'LATEST_CREATED' as PostsSortEnum })
}
