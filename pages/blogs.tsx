import ListLayout from '@/layouts/ListLayout'
import { GetServerSideProps, InferGetStaticPropsType } from 'next'
import { ComponentProps } from 'react'
import * as BlogPage from './blogs/pages/[page]'

export const getServerSideProps: GetServerSideProps<{
  initialPosts: ComponentProps<typeof ListLayout>['posts']
  initialPagination: ComponentProps<typeof ListLayout>['pagination']
}> = async (context) => {
  return BlogPage.getServerSideProps({ ...context, params: { page: '1' } })
}

export default function Blogs({
  initialPosts,
  initialPagination,
}: InferGetStaticPropsType<typeof getServerSideProps>) {
  return BlogPage.default({ initialPosts, initialPagination })
}
