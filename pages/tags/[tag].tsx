import ListLayout from '@/layouts/ListLayout'
import { GetServerSideProps, InferGetStaticPropsType } from 'next'
import { ComponentProps } from 'react'
import * as TagPage from './[tag]/pages/[page]'

export const getServerSideProps: GetServerSideProps<{
  tag: string
  initialPosts: ComponentProps<typeof ListLayout>['posts']
  initialPagination: ComponentProps<typeof ListLayout>['pagination']
}> = async (context) => {
  const tag = context.params.tag
  return TagPage.getServerSideProps({ ...context, params: { tag, page: '1' } })
}

export default function Tag({
  tag,
  initialPosts,
  initialPagination,
}: InferGetStaticPropsType<typeof getServerSideProps>) {
  return TagPage.default({ tag, initialPosts, initialPagination })
}
