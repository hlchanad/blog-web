import { TagSEO } from '@/components/SEO'
import siteMetadata from '@/data/siteMetadata'
import ListLayout from '@/layouts/ListLayout'
import { GetServerSideProps, InferGetServerSidePropsType } from 'next'
import { parseInt } from 'lodash'
import { PostsQuery } from '@/graphql/posts/queries'
import { PostDto } from '@/dtos'
import { ComponentProps, useEffect, useState } from 'react'
import { config } from '@/helpers'

const POSTS_PER_PAGE = config<number>('post.num-posts-per-page')

export const getServerSideProps: GetServerSideProps<{
  tag: string
  initialPosts: ComponentProps<typeof ListLayout>['posts']
  initialPagination: ComponentProps<typeof ListLayout>['pagination']
}> = async ({ params }) => {
  const slug = params.tag as string
  const pageNumber = parseInt(params.page as string)

  const result = await PostsQuery({
    filter: { and: { tags: [slug] } },
    offset: (pageNumber - 1) * POSTS_PER_PAGE,
    limit: POSTS_PER_PAGE,
  })

  const posts = result.edges.map((post) => PostDto.toPostFrontMatter(post))
  const pagination = {
    currentPage: pageNumber,
    totalPages: Math.ceil(result.totalCount / POSTS_PER_PAGE),
  }

  return {
    props: {
      tag: slug,
      initialPosts: posts,
      initialPagination: pagination,
    },
  }
}

export default function TagPage({
  tag,
  initialPosts,
  initialPagination,
}: InferGetServerSidePropsType<typeof getServerSideProps>) {
  const [posts, setPosts] = useState(initialPosts)
  const [pagination, setPagination] = useState(initialPagination)
  const [searchValue, setSearchValue] = useState('')

  useEffect(() => {
    setPosts(initialPosts)
    setPagination(initialPagination)
  }, [initialPosts, initialPagination])

  const onSearch = async (searchValue: string) => {
    setSearchValue(searchValue)

    if (!searchValue) {
      setPosts(initialPosts)
      setPagination(initialPagination)
      return
    }

    const result = await PostsQuery({
      filter: {
        and: { tags: [tag] },
        or: {
          author: searchValue,
          category: searchValue,
          summary: searchValue,
          tags: [searchValue],
          title: searchValue,
        },
      },
      limit: POSTS_PER_PAGE,
    })

    setPosts(result.edges.map((post) => PostDto.toPostFrontMatter(post)))
    setPagination({
      currentPage: 1,
      totalPages: Math.ceil(result.totalCount / POSTS_PER_PAGE),
    })
  }

  const title = tag[0].toUpperCase() + tag.split(' ').join('-').slice(1)

  return (
    <>
      <TagSEO
        title={`${tag} - ${siteMetadata.title}`}
        description={`${tag} tags - ${siteMetadata.author}`}
      />
      <ListLayout
        posts={posts}
        pagination={pagination}
        title={title}
        baseUrl={`/tags/${tag}`}
        searchValue={searchValue}
        onSearch={onSearch}
      />
    </>
  )
}
