import { MDXLayoutRenderer } from '@/components/MDXComponents'
import { getMdx } from '@/lib/mdx'
import { GetServerSideProps, InferGetServerSidePropsType } from 'next'
import { AuthorFrontMatter } from 'types/AuthorFrontMatter'
import { UserQuery } from '@/graphql/posts/queries'
import { UserDto } from '@/dtos'

const DEFAULT_LAYOUT = 'AuthorLayout'

// @ts-ignore
export const getServerSideProps: GetServerSideProps<{
  authorMdx: { mdxSource: string; frontMatter: AuthorFrontMatter }
}> = async ({ params }) => {
  const username = params.username as string

  const author = await UserQuery(username)

  const authorMdx = await getMdx(UserDto.toMdx(author), author.id)

  return { props: { authorMdx } }
}

export default function About({
  authorMdx,
}: InferGetServerSidePropsType<typeof getServerSideProps>) {
  const { mdxSource, frontMatter } = authorMdx

  return (
    <MDXLayoutRenderer
      layout={frontMatter.layout || DEFAULT_LAYOUT}
      mdxSource={mdxSource}
      frontMatter={frontMatter}
    />
  )
}
