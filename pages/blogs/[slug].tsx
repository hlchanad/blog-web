import PageTitle from '@/components/PageTitle'
import { MDXLayoutRenderer } from '@/components/MDXComponents'
import { getMdx } from '@/lib/mdx'
import { GetServerSideProps, InferGetServerSidePropsType } from 'next'
import { AuthorFrontMatter } from 'types/AuthorFrontMatter'
import { PostFrontMatter } from 'types/PostFrontMatter'
import { Toc } from 'types/Toc'
import { PostQuery } from '@/graphql/posts/queries'
import { PostDto, UserDto } from '@/dtos'

const DEFAULT_LAYOUT = 'PostLayout'

// @ts-ignore
export const getServerSideProps: GetServerSideProps<{
  post: { mdxSource: string; toc: Toc; frontMatter: PostFrontMatter }
  authorDetails: AuthorFrontMatter[]
  prev?: { slug: string; title: string }
  next?: { slug: string; title: string }
}> = async ({ params }) => {
  const slug = params.slug as string

  const postModel = await PostQuery(slug)

  const post = await getMdx(PostDto.toMdx(postModel), slug)
  const author = await getMdx(UserDto.toMdx(postModel.author), postModel.author.id)

  const prev: { slug: string; title: string } = postModel.previous
    ? { slug: postModel.previous.slug, title: postModel.previous.title }
    : null
  const next: { slug: string; title: string } = postModel.next
    ? { slug: postModel.next.slug, title: postModel.next.title }
    : null

  return {
    props: {
      post,
      authorDetails: [author.frontMatter],
      prev,
      next,
    },
  }
}

export default function Blog({
  post,
  authorDetails,
  prev,
  next,
}: InferGetServerSidePropsType<typeof getServerSideProps>) {
  const { mdxSource, toc, frontMatter } = post

  return (
    <>
      {'draft' in frontMatter && frontMatter.draft !== true ? (
        <MDXLayoutRenderer
          layout={frontMatter.layout || DEFAULT_LAYOUT}
          toc={toc}
          mdxSource={mdxSource}
          frontMatter={frontMatter}
          authorDetails={authorDetails}
          prev={prev}
          next={next}
        />
      ) : (
        <div className="mt-24 text-center">
          <PageTitle>
            Under Construction{' '}
            <span role="img" aria-label="roadwork sign">
              🚧
            </span>
          </PageTitle>
        </div>
      )}
    </>
  )
}
