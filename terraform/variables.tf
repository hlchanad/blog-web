variable "env" {
  type = string
}

variable "application" {
  type = string
  default = "blog-web"
}

# --- provider --------
variable "region" {
  description = "The region used to launch this module resources."
  type = string
}

variable "profile" {
  description = "The profile name as set in the shared credentials file. If not set, it will be sourced from the ALICLOUD_PROFILE environment variable."
  type = string
  default = ""
}

# --- domain record -------
variable "custom_domain" {
  description = "Your custom domain"
  type        = string
  default     = "example.com"
}

variable "custom_domain_zone_name" {
  description = "The Route53 zone name of the custom domain"
  type        = string
  default     = "example.com."
}

variable "acm_cert" {
  description = "Your acm cert"
  type        = string
  default     = "*.example.com"
}
