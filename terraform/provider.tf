# Main region where the resources should be created in
# (Should be close to the location of your viewers)
provider "aws" {
  region = var.region
}

# Provider used for creating the Lambda@Edge function which must be deployed
# to us-east-1 region (Should not be changed)
provider "aws" {
  alias  = "global_region"
  region = "us-east-1"
}
