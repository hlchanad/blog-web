locals {
  app = "${var.application}-${var.env}"
}

data "aws_route53_zone" "custom_domain_zone" {
  name = var.custom_domain_zone_name
}

data "aws_acm_certificate" "acm_cert"{
  provider = aws.global_region

  domain = var.acm_cert
  statuses = ["ISSUED"]
}

resource "aws_route53_record" "cloudfront_alias_domain" {
  zone_id = data.aws_route53_zone.custom_domain_zone.zone_id
  name = var.custom_domain
  type = "A"

  alias {
    name = module.tf_next.cloudfront_domain_name
    zone_id = module.tf_next.cloudfront_hosted_zone_id
    evaluate_target_health = false
  }
}

module "tf_next" {
  source = "milliHQ/next-js/aws"

  providers = {
    aws.global_region = aws.global_region
  }

  deployment_name = local.app
  next_tf_dir = "../.next-tf"

  cloudfront_aliases = [var.custom_domain]
  cloudfront_acm_certificate_arn = data.aws_acm_certificate.acm_cert.arn
}
