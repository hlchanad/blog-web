export const graphql = {
  url: process.env.GRAPHQL_URL ?? 'http://localhost:4000/dev/graphql',
}
