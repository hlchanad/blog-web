export type AuthorFrontMatter = {
  layout?: string
  username: string
  name: string
  avatar: string
  occupation?: string
  company?: string
  description: string
  email: string
  contacts: Array<{
    type: string
    url: string
  }>
}
