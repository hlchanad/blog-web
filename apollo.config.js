/**
 * @type {import('apollo').ApolloConfig}
 **/
module.exports = {
  client: {
    service: {
      name: 'blog-api',
      url: 'http://localhost:4000/dev/graphql',
    },
    includes: [
      './components/**/*.ts',
      './components/**/*.tsx',
      './layouts/**/*.ts',
      './layouts/**/*.tsx',
      './pages/**/*.ts',
      './pages/**/*.tsx',
    ],
  },
}
