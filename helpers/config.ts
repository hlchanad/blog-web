import { get } from 'lodash'
import * as AppConfigs from '../config'

export function config<T>(path: string, _default?: T): T {
  return get(AppConfigs, path, _default)
}
