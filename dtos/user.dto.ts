import { User } from '@/graphql/types'
import { dump } from 'js-yaml'

export function toMdx(user: User): string {
  const yaml = dump({
    name: [user.firstname, user.lastname].join(' ').trim(),
    username: user.username,
    description: user.description,
    avatar: user.avatar,
    contacts: user.contacts,
    email: user.email,
  })
  return `---\n${yaml}\n---\n\n${user.description}`
}
