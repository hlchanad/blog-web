import { Post } from '@/graphql/types'
import { dump } from 'js-yaml'
import moment from 'moment'
import { PostFrontMatter } from '../types/PostFrontMatter'

export function toMdx(post: Post): string {
  const yaml = dump({
    title: post.title,
    date: moment(post.createdAt).format('YYYY-MM-DD'),
    lastmod: moment(post.updatedAt).format('YYYY-MM-DD'),
    tags: post.tags.map(({ name }) => name),
    draft: false,
    summary: post.summary,
    images: [post.banner],
    authors: [post.author.id],
  })
  return `---\n${yaml}\n---\n\n![post-banner](${post.banner})\n\n${post.content}`
}

export function toPostFrontMatter(post: Post): PostFrontMatter {
  return {
    title: post.title,
    summary: post.summary,
    date: post.createdAt,
    tags: post.tags.map((tag) => tag.name),
    lastmod: post.updatedAt,
    slug: post.slug,
  }
}
